"""Пара примеров использования менеджера параметров"""

# После этого импорта произойдет регистрация параметров и мы сможем к ним обращаться через ParamsManager
# Этот импорт можно вынести в __init__.py модуля, где лежит ParamsManager. Тогда, после импорта ParamsManager,
# нам сразу будут доступны все параметры системы
import params_example

from manager import ParamsManager
from params_example import SystemType


if __name__ == '__main__':
    # Получаем все параметры
    print(ParamsManager.get_registered_params())

    # Получение значения параметра
    print(ParamsManager.ParamA, ParamsManager['ParamA'])

    # Таким образом можно задать значение параметра. Но, поскольку тут отсутствует сервис параметров,
    # значение не будет устанановлено и параметр будет иметь свое значение по-умолчанию
    # ParamsManager.ParamA = 1
    # ParamsManager['ParamA'] = 1

    try:
        ParamsManager.ParamA = 'abc'
    except TypeError:
        print('Пример ошибке, когда параметру, принимающему значение int пытаемся задать значение другого типа')

    # Получение параметра по системе
    print(ParamsManager.search(system=SystemType.A))

    # Поиск по названию
    print(ParamsManager.search(search_string='ParamB'))

    # Поиск по описанию
    print(ParamsManager.search(search_string='Строковый параметр'))

