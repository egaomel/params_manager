"""Тестирование менеджера параметров"""

from unittest import TestCase

import datetime
from unittest.mock import patch, call
from parameterized import param, parameterized

from manager import ParamsManager, BaseParams
from validators import Validators, ParamValidationError


class TestParamsManager(TestCase):

    def setUp(self):
        [ParamsManager.__dict__['_params'].pop(key) for key in list(ParamsManager.__dict__['_params'])]

    @parameterized.expand([
        param(
            value_type=int,
            default_value='0',
            current_value='1'
        ),
        param(
            value_type=str,
            default_value='0',
            current_value='1'
        ),
        param(
            value_type=float,
            default_value='0.0',
            current_value='1.01'
        ),
    ])
    def test_1(self, value_type, default_value, current_value):
        """Тестирование получения параметра"""

        @ParamsManager.register(group='test')
        class TestParam(BaseParams):
            _ID = 'Mercury.Test'
            _TYPE = value_type
            _DEFAULT_VALUE = default_value

        with patch('manager.get_param') as mock_get_param:
            mock_get_param.return_value = current_value

            self.assertEqual(ParamsManager.TestParam, value_type(current_value))

    def test_2(self):
        """Тестирование получения параметра через переопределенную функцию"""

        @ParamsManager.register(group='test')
        class TestParam(BaseParams):
            _ID = 'Mercury.Test'
            _TYPE = datetime.datetime
            _DEFAULT_VALUE = '2018-06-01T00:00:00.000000'

            @classmethod
            def from_string(cls, value):
                """Получение даты из строки."""
                return datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%S.%f')

            @classmethod
            def _to_string(cls, value):
                """Получение строки из даты."""
                return datetime.datetime.strftime(value, '%Y-%m-%dT%H:%M:%S.%f')

        current_value = '2018-03-02T00:00:00.000000'

        with patch('manager.get_param') as mock_get_param:
            mock_get_param.return_value = current_value

            result = ParamsManager.TestParam
            self.assertEqual(result, datetime.datetime.strptime(current_value, "%Y-%m-%dT%H:%M:%S.%f"))

    @patch('manager.history.modified')
    def test_3(self, _):
        """Тестирование задания параметра"""

        @ParamsManager.register(group='test')
        class TestParam(BaseParams):
            _ID = 'Mercury.Test'
            _TYPE = int
            _DEFAULT_VALUE = '1'
            _VALIDATORS = set()

        with patch('manager.set_param') as mock_set_param:
            ParamsManager.TestParam = 2
            mock_set_param.assert_has_calls(
                [call('TestParam', '2')]
            )

    def test_4(self):
        """Тестирование исключений при взятии или задании несуществующего параметра."""
        with self.assertRaises(KeyError):
            ParamsManager.Test

        with self.assertRaises(KeyError):
            ParamsManager['Test']

        with self.assertRaises(KeyError):
            ParamsManager.Test = 0

        with self.assertRaises(KeyError):
            ParamsManager['Test'] = 0

    def test_5(self):
        """Тестирование исключений когда параметр не наследник BaseParam."""

        class TestParam:
            _ID = 'Mercury.Test'
            _TYPE = int
            _DEFAULT_VALUE = '0'

        with self.assertRaises(AssertionError):
            ParamsManager.register(group='test')(TestParam)

    def test_6(self):
        """Тестирование исключений при повторной регистрации параметра."""

        class TestParam(BaseParams):
            _ID = 'Mercury.Test'
            _TYPE = int
            _DEFAULT_VALUE = '0'

        ParamsManager.register(group='test')(TestParam)

        with self.assertRaises(AssertionError):
            ParamsManager.register(group='test')(TestParam)

    def test_7(self):
        """Тестирование исключений при непроинициализированном поле класса."""

        @ParamsManager.register(group='test')
        class TestParam(BaseParams):
            _TYPE = int
            _DEFAULT_VALUE = '0'

        with self.assertRaises(AssertionError):
            ParamsManager.TestParam

    def test_8(self):
        """Тестирования возвращения параметра по умолчанию при пустом ответе от сервиса параметров."""
        default_value = 1

        @ParamsManager.register(group='test')
        class TestParam(BaseParams):
            _TYPE = int
            _ID = 'Test'
            _DEFAULT_VALUE = default_value

        with patch('manager.set_param') as mock_get_param:
            mock_get_param.return_value = None

            self.assertEqual(ParamsManager.TestParam, default_value)

    def test_9(self):
        """
        Тестирования возвращения параметра по умолчанию
        при невозможности привести ответ сервиса параметров к указанному типа
        """
        default_value = 1

        @ParamsManager.register(group='test')
        class TestParam(BaseParams):
            _TYPE = int
            _ID = 'Test'
            _DEFAULT_VALUE = default_value
            _VALIDATORS = set()
            _SYSTEM = 1

        with patch('manager.set_param') as mock_get_param:
            mock_get_param.return_value = None

            result = ParamsManager.get_multiple_params()

            self.assertEqual(result[0]['CurrentValue'], default_value)

    def test_10(self):
        """Установка параметру значения неподдерживаемого типа."""

        @ParamsManager.register(group='test')
        class TestParam(BaseParams):
            _TYPE = int
            _ID = 'Test'
            _DEFAULT_VALUE = '0'

        with self.assertRaises(Exception):
            ParamsManager.TestParam = {1: 2}

    def test_11(self):
        """Установка значения, при недоступном сервисе параметров."""

        @ParamsManager.register(group='test')
        class TestParam(BaseParams):
            _TYPE = int
            _ID = 'Test'
            _DEFAULT_VALUE = '0'

        with self.assertRaises(Exception), patch('manager.invoke_async_method') as param_:
            param_.side_effect = Exception()

            ParamsManager.TestParam = 1

    def test_12(self):
        """Возникновение ошибки при некорректно заданном значении по умолчанию."""

        with self.assertRaises(ParamValidationError):
            @ParamsManager.register('')
            class TestParam(BaseParams):
                _TYPE = int
                _ID = 'Test'
                _DEFAULT_VALUE = '1'
                _VALIDATORS = {Validators.in_range(2, 3)}

        with self.assertRaises(TypeError):
            @ParamsManager.register('')
            class TestParam2(BaseParams):
                _TYPE = int
                _ID = 'Test'
                _DEFAULT_VALUE = 'q'
                _VALIDATORS = {Validators.in_range(2, 3)}
