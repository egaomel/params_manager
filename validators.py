"""
Валидаторы параметров, накладываемые при установке его значения
"""
__author__ = 'ea.omelyanenko'


def messages(error_message, user_message):
    """
    Декоратор, задающий сообщение об ошибке, при непрохождении валидации и
    сообщение для пользователя, описывающее, какие значения может принимать параметр
    :param str error_message: текст сообщения об ошибке
    :param str user_message: текст сообщения для пользователя
    """
    def wrapper(func):
        setattr(func, 'error_message', error_message)
        setattr(func, 'user_message', user_message)
        return func
    return wrapper


class Validators:
    """Класс содержащий разного рода валидаторы."""

    @staticmethod
    def in_range(min_=None, max_=None):
        """
        Валидация вхождения значения в числовой промежуток.
        ВАЖНО! Граничные условия входят в промежуток.
        ВАЖНО! Если не указаны оба параметра, то будет ошибка, если не указан min_ - условие будет вида val <= max_,
        если не указан max_ - условие будет вида val >= min_

        :param float min_: Минимальное значение.
        :param float max_: Максимальное значение.
        :rtype: bool
        """

        if min_ is None and max_ is None:
            raise Exception('Для валидатора in_range необходимо указать минимальное и/или максимальное значение')

        elif min_ is not None and max_ is not None:
            message = 'в интервале от {} до {}'.format(min_, max_)
            validator = lambda val: min_ <= val <= max_

        elif min_ is not None:
            message = 'больше или равные {}'.format(min_)
            validator = lambda val: min_ <= val

        else:
            message = 'меньше или равные {}'.format(max_)
            validator = lambda val: val <= max_

        return messages(
            error_message='Указано недопустимое значение параметра. Допустимы значения {}'.format(message),
            user_message='Параметр может принимать значения {}'.format(message)
        )(validator)


class ParamValidationError(Exception):
    """Ошибка валидации параметра. Может быть указано имя параметра и причина."""

    def __init__(self, parameter_name, reason):
        """
        Ошибка валидации.

        :param str parameter_name: Название параметра.
        :param str reason: Причина ошибки.
        """

        self.parameter_name = parameter_name
        self.reason = reason

    def __str__(self):
        return "Ошибка '{}' при валидации параметра: {}".format(
            self.reason, self.parameter_name
        )
