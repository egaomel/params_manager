"""
Менеджер параметров.

Данный код предоставляет возможность использовать менеджр параметров.

    Основные задачи:
        * Реализовать единую точку доступа для всех используемых в системе параметров и удобный доступ к ним.

        * Следить, чтобы у нас не было зарегистрировано несколько параметров с одним именем

        * Возвращать\записывать значение на сервис параметров (внутряння штука, там хранятся пары ключ: значение), если
        сервис параметров недоступен - возвращать значение по-умолчанию

    Всякие фичи:
        * В менеджере нельзя зарегистрировать параметр с некорректными значениями
        * Валидация новых значений с помощью кастомных валидаторов
        * Разделение параметров по системам
        * Разделение параметров по группам
        * Получение всех параметров, их поиск по системе, группе, названию и т.п.
        * т.д.

"""


from validators import ParamValidationError

from patch import *


class _MetaManager(type):
    """Метакласс для контроля получения и установки аттрибутов"""

    def __getitem__(cls, key):
        """Получить значение с помощью синтаксиса словаря"""
        return getattr(cls, key)

    def __getattr__(cls, name):
        """Получение параметра по имени"""

        try:
            cls._params[name]
        except KeyError:
            raise KeyError('Попытка получить несуществующий параметр: {}'.format(name))

        return cls._params[name].get_current_value()

    def __setitem__(cls, key, value):
        """Установить значение с помощью синтаксиса словаря"""
        setattr(cls, key, value)

    def __setattr__(cls, name, val):
        """Установка нового значения параметру"""

        try:
            cls._params[name]
        except KeyError:
            raise KeyError('Попытка установить значение несуществующему параметру: {}'.format(name))

        cls._params[name].set_current_value(val)


class ParamsManager(metaclass=_MetaManager):
    """Менеджер параметров"""

    # Список доступных параметров
    _params = {}

    @classmethod
    def get_registered_params(cls):
        """Получить список всех зарегистрированных параметров"""
        return cls._params

    @staticmethod
    def register(group):
        if group and not isinstance(group, str):
            raise ValueError(
                'Неверный тип группы {}, допустим тип str. Уровни разделаются с помощью символа "/"'.format(
                    group
                )
            )

        def decorator(param):
            param._GROUP = group

            ParamsManager._register_param(param)

        return decorator

    @classmethod
    def _register_param(cls, param):
        """
        Декоратор для регистрации параметра. Повторная регистрация параметров
        с одинаковым именем запрещена!

        :param BaseParams param: Параметр который будет зарегестрирован в менеджере.
            Любой параметр должен быть наследником BaseParams.

            После регистрации к параметру можно обратиться следующим образом:
            Example:
            >>> value = ParamsManager.Parameter
            >>> ParamsManager.Parameter = 'new value'
        """

        assert param.__base__ == BaseParams, (
            'Класс параметра должен быть наследником BaseParams'
        )

        assert param.__name__ not in cls._params, (
            'Попытка зарегистрировать параметр дважды: {}'.format(
                param.__name__
            )
        )

        assert param.from_string(param.get_default_value()) is not None, (
            'Параметр по умолчанию не может быть приведен к типу параметра'
        )

        param.validate(param.from_string(param.get_default_value()))

        cls._params[param.__name__] = param

    @classmethod
    def get_multiple_params(cls, params_ids=None):
        """
        Получить информацию о параметрах

        :param None|List[str] params_ids: Список идентификаторов параметров. При None - возвращаются все параметры
        :rtype: List[dict]
        :return: Список словарей вида:
            {
                'Id': ...,           # Название параметра в сервисе настроек
                'Description': ...,  # Подробное описание параметра
                'Type': ...,         # Тип параметра, может быть также функцией
                'DefaultValue': ..., # Значение по умолчанию
                'CurrentValue': ..., # Значение полученное с сервиса параметров на момент вызова метода
                'Limitation': ...,   # Ограничения значения, принимаемого параметром
                'Group': ...,        # Группа параметра
                'System': ...,       # Система параметра
            }
        """

        if params_ids is None:
            params_ids = [param for param in cls._params]
        elif not all([param in cls._params for param in params_ids]):
            raise Exception('Запрошен несуществующий параметр')

        values_from_service = get_multiple_params(params_ids) or []

        values_from_service = {
            value.Get('Key'): value.Get('Value') for value in values_from_service
        }

        result = []

        for param_id in params_ids:
            # Условие позволяет не выводить в диагностику параметры для отключенных систем (у которых _SYSTEM < 0)
            if cls._params[param_id].get_system() >= 0:
                result.append(
                    cls._params[param_id].to_dict(
                        # если параметра нет на сервисе параметров, то используем его значение по-умолчанию
                        current_value=values_from_service[param_id] if param_id in values_from_service else None,
                        default_value_as_current=True if param_id not in values_from_service else False
                    )
                )

        return result

    @classmethod
    def search(cls, system=None, search_string=None):
        """
        Получить идентификаторы параметров, подходящих под фильтр
        :param int|None system: Система
        :param str|None search_string: Строка для поиска
        :rtype List[str]:
        """

        result = []

        for param in cls._params.values():
            if system is not None and param.get_system() != system:
                continue

            if search_string is not None:
                prepared_string_for_search = '{}{}{}'.format(
                    param.get_id(), param.get_description(), param.get_group()
                ).lower()
                # найдена ли в строке подстрока
                if prepared_string_for_search.find(search_string.lower()) == -1:
                    continue

            result.append(param.get_id())

        return result


class BaseParams:
    """Базовый класс для всех параметров"""

    # Название параметра в сервисе настроек
    _ID = NotImplemented

    # Тип параметра
    _TYPE = NotImplemented

    # Валидаторы параметра
    _VALIDATORS = set()

    # Значение по умолчанию. Используется если при запросе к БЛ произошла ошибка,
    # а также если функция _TYPE вернула ошибка. Значение должно быть задано в виде строки.
    _DEFAULT_VALUE = NotImplemented

    # Группа параметра
    _GROUP = NotImplemented

    # Система параметра
    _SYSTEM = NotImplemented

    # Описание ошибки с не переопределенным параметром
    __NOT_IMPLEMENTED_ERROR = 'Поле класса {} должно быть переопределено'

    @classmethod
    def from_string(cls, value):
        """
        Преобразовать параметр к необходимому типу из строки.
        Для простых типов, таких как int, float, str работает из коробки.

        :param str value: Строка которая будет преобразована к необходимому типу.
        """

        try:
            return cls._TYPE(value)
        except Exception:
            raise TypeError(
                'Значение \'{}\' не может быть приведено к типу параметра \'{}\''.format(value, cls._TYPE.__name__)
            )

    @classmethod
    def get_current_value(cls):
        """Получить текущее значение параметра. Если сервис параметров недоступен - вернёт значение по-умолчанию"""
        assert cls._TYPE is not NotImplemented, cls.__NOT_IMPLEMENTED_ERROR.format("_TYPE")
        assert cls._ID is not NotImplemented, cls.__NOT_IMPLEMENTED_ERROR.format("_ID")
        assert cls._DEFAULT_VALUE is not NotImplemented, cls.__NOT_IMPLEMENTED_ERROR.format("_DEFAULT_VALUE")

        return cls.from_string(
            get_param(cls.get_id())
            or cls._DEFAULT_VALUE
        )

    @classmethod
    def set_current_value(cls, value):
        """
        Установить новое значение параметра
        :param value: Новое значение параметра.
        """

        old_value = cls.get_current_value()
        value = cls.from_string(value)

        cls.validate(value)

        try:
            set_param(
                cls.get_id(),
                cls._to_string(value),
            )

        except Exception:
            raise RuntimeError('Ошибка сервиса параметров')
        else:

            history.modified(
                cls.get_id(),
                cls._to_string(old_value),
                cls._to_string(value)
            )

    @classmethod
    def _to_string(cls, value):
        """
        Преобразовать параметр к нужному типу.

        :param any value: Параметр.
        :rtype: str
        :return: Строковой представление параметра.
        """

        return str(value)

    @classmethod
    def validate(cls, value):
        """
        Валидация полученного значения валидаторами параметра.
        :param value: Значение для валидации
        """
        for validator in cls._VALIDATORS:
            if not validator(value):
                raise ParamValidationError(cls.__name__, validator.error_message)

    @classmethod
    def to_dict(cls, current_value=None, default_value_as_current=False):
        """
        Получить параметры сервиса в виде словаря
        :param str|None current_value: Текущее значение параметра.
        Если None и не указан флаг default_value_as_current - будет получено с сервиса параметров
        :param bool default_value_as_current: Использовать значение по-умолчанию, как текущее
        """

        if current_value is None:
            current_value = cls.get_current_value() if not default_value_as_current else cls.get_default_value()

        return {
            'Id': cls.get_id(),
            'Description': cls.get_description(),
            'Type': cls._TYPE.__name__,
            'DefaultValue': cls.get_default_value(),
            'CurrentValue': current_value,
            'Limitation': [validator.user_message for validator in cls._VALIDATORS],
            'Group': cls.get_group(),
            'System': cls.get_system()
        }

    @classmethod
    def get_description(cls):
        """
        Получить описание параметра
        """
        return cls.__doc__.strip() if cls.__doc__ else None

    @classmethod
    def get_id(cls):
        """
        Получить идентификатор параметра
        """
        return cls.__name__

    @classmethod
    def get_system(cls):
        """
        Получить систему параметра
        """
        return cls._SYSTEM

    @classmethod
    def get_group(cls):
        """
        Получить идентификатор параметра
        """
        return cls._GROUP

    @classmethod
    def get_default_value(cls):
        """
        Получить идентификатор параметра
        """
        return cls._DEFAULT_VALUE
