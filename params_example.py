"""Примеры параметров в том виде, в котором они используются на бою"""

import datetime
from manager import ParamsManager, BaseParams
from validators import Validators


class SystemType:
    """Тип системы, для которой используется параметр"""
    A = 0
    B = 1
    C = 2


@ParamsManager.register(group='Параметр/Будет/В/Этой/Группе')
class ParamA(BaseParams):
    """Максимальное кол-во чего-нибудь, используемого в системе А."""

    _ID = 'ParamA'
    _TYPE = int
    _DEFAULT_VALUE = '10000'
    _VALIDATORS = {Validators.in_range(min_=0)}
    _SYSTEM = SystemType.A


@ParamsManager.register(group='Группа/Совсем/Другого/Параметра')
class ParamB(BaseParams):
    """Дата, с которой происходит что-нибудь в системе B"""

    _ID = 'ParamB'
    _TYPE = datetime.datetime
    _DEFAULT_VALUE = '2019-06-01T00:00:00.000000'
    _SYSTEM = SystemType.B

    @classmethod
    def from_string(cls, value):
        """Получение даты из строки."""
        try:
            return datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%S.%f')
        except Exception:
            raise TypeError(
                'Значение \'{}\' не может быть приведено к типу параметра \'{}\''.format(value, cls._TYPE.__name__)
            )

    @classmethod
    def _to_string(cls, value):
        """Получение строки из даты."""
        return datetime.datetime.strftime(value, '%Y-%m-%dT%H:%M:%S.%f')


@ParamsManager.register(group='Группа/Третьего/Параметра')
class ParamC(BaseParams):
    """Строковый параметр, например url метода, которому мы будем что-то отправлять в системе С"""

    _ID = 'ParamC'
    _TYPE = str
    _DEFAULT_VALUE = 'mercury|Test.DeliverySuccess'
    _SYSTEM = SystemType.C
